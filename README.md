# Logická hra Lodě

Jedná se o logickou hru s názvem Lodě (anglicky Battleships), která mi poslouží jako maturitní práce z Informatiky. 

Zde naleznete samotný kód a složku assets, která se skládá z dalších složek a je potřebná pro spuštění hry. 

[Odkaz na prezentaci](https://docs.google.com/presentation/d/1DPhSg9oLgg8zjCipXUKFTWLzRe-FAlsPbFnQnjdd4rY/edit#slide=id.p)
