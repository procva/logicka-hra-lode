#Module imports
import pygame
import random
import sys

#Module initialization
pygame.init()

#Game assets and objects
class Ship:
    def __init__(self, name, img, pos, size):
        self.name = name
        self.pos = pos
        #atributy pro vertikální pozice lodě
        self.vImage = loadImage(img, size)
        self.vImageWidth = self.vImage.get_width()
        self.vImageHeight = self.vImage.get_height()
        self.vImageRect = self.vImage.get_rect()
        self.vImageRect.topleft = pos
        #atributy pro horizontální pozice lodě
        self.hImage = pygame.transform.rotate(self.vImage, -90)
        self.hImameWidth = self.hImage.get_width()
        self.hImageHeight = self.hImage.get_height()
        self.hImageRect = self.hImage.get_rect()
        self.hImageRect.topleft = pos
        #určení proměn a jejich hodnot
        self.image = self.vImage
        self.rect = self.vImageRect
        self.rotation = False
        #souvisí s def selectShipAndMove
        self.active = False

    def selectShipAndMove(self):
        #tato metoda vezme loď a umístí ji tam kam ukazuje myš
        while self.active == True:
            self.rect.center = pygame.mouse.get_pos()
            updateGameScreen(GAMESCREEN)
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if not self.checkForCollisions(pFleet):
                        if event.button == 1:
                            self.hImageRect.center = self.vImageRect.center = self.rect.center
                            self.active = False

                        if event.button == 3:
                            self.rotateShip()

    def rotateShip(self, doRotation=False):
        #Tato metoda otočí lodě o 90 stupňů
        if self.active or doRotation == True:
            if self.rotation == False:
                self.rotation = True
            else:
                self.rotation = False
            self.switchImageAndRect()

    def switchImageAndRect(self):
        #tato metoda bude měnit pozice lodí z horizontální na vertikální a naopak
        if self.rotation == True:
            self.image = self.hImage
            self.rect = self.hImageRect
        else:
            self.image = self.vImage
            self.rect = self.vImageRect
        self.hImageRect.center = self.vImageRect.center = self.rect.center 

    def checkForCollisions(self, shiplist):
        #Tato metoda zajistí, že se lodě nebudou překrývat; 
        slist = shiplist.copy()
        slist.remove(self)
        for item in slist:
            if self.rect.colliderect(item.rect):
                return True
        return False

    def checkForRotateCollisions(self, shiplist):
        #tato metoda zkontroluje jestli může loď otočit, aby nedošlo ke kolizi s flotilou
        slist = shiplist.copy()
        slist.remove(self)
        for ship in slist:
            if self.rotation == True:
                if self.vImageRect.colliderect(ship.rect):
                    return True
            else:
                if self.hImageRect.colliderect(ship.rect):
                    return True
        return False

    def returnToDefaultPosition(self):
        #Tato metoda vrátí loď na svou původní pozici
        if self.rotation == True:
            self.rotateShip(True)
            
        self.rect.topleft = self.pos
        self.hImageRect.center = self.vImageRect.center = self.rect.center

    def snapToGridEdge(self, gridCoords): 
        #zarovnání lodí do mřížek
        if self.rect.topleft !=self.pos: #pokud se s lodi pohlo
            #50jevzdálenostodmřížkykdyseloďotočí
            if self.rect.left > gridCoords[0][-1][0] + CELLSIZE or \
                self.rect.right < gridCoords[0][0][0] or \
                self.rect.top > gridCoords[-1][0][1] + CELLSIZE or \
                self.rect.bottom < gridCoords[0][0][1]:
                self.returnToDefaultPosition() 
                #lod je pravo mimo prizku, vlevo, pod nebo nad mrizkou
            

            elif self.rect.right > gridCoords[0][-1][0] + CELLSIZE:
                self.rect.right = gridCoords[0][-1][0] + CELLSIZE
            elif self.rect.left < gridCoords[0][0][0]:
                self.rect.left = gridCoords[0][0][0]
            elif self.rect.top < gridCoords[0][0][1]:
                self.rect.top = gridCoords[0][0][1]
            elif self.rect.bottom > gridCoords[-1][0][1] + CELLSIZE:
                self.rect.bottom = gridCoords[-1][0][1] + CELLSIZE
            self.vImageRect.center = self.hImageRect.center = self.rect.center
            #pokud lod presahuje mrizku jenom castecne, vrati lod do kraje mrizky
            
    def snapToGrid(self, gridCoords):
        #zarovnání do mřížky - konkrétní políčka
        for rowX in gridCoords:
            for cell in rowX:
                if self.rect.left >= cell[0] and self.rect.left < cell[0] + CELLSIZE \
                    and self.rect.top >= cell[1] and self.rect.top < cell[1] + CELLSIZE:
                    if self.rotation == False: 
                        self.rect.topleft = (cell[0] + (CELLSIZE - self.image.get_width())//2, cell[1]) #vertikalni lod vycentruju na stred bunky
                    else:
                        self.rect.topleft = (cell[0], cell[1] + (CELLSIZE - self.image.get_height())//2) #horizontalni lod vycentruju na stred bunky
                        
        self.hImageRect.center = self.vImageRect.center = self.rect.center      
        #projde vsechny bunky, zjisti ve ktere se nachazi horni levy roh lode a zarovna lod do mrizky

    def draw(self, window):
        #tato metoda vykreslí loď na obrazovku
        window.blit(self.image, self.rect)
        #pygame.draw.rect(window, (255, 0, 0), self.rect, 1) #obdelníková ovlast bude mít červený okraj tloušťky 1 pixel

class Button:
    def __init__(self, image, size, pos, msg):
        self.name = msg
        self.image = image
        self.imageLarger = self.image
        self.imageLarger = pygame.transform.scale(self.imageLarger, (size[0] + 10, size[1] + 10))
        self.rect = self.image.get_rect()
        self.rect.topleft = pos
        self.active = False

        self.msg = self.addText(msg)
        self.msgRect = self.msg.get_rect(center=self.rect.center)
        

    def addText(self, msg):
        #tato metoda přidá na tlačítko text
        font = pygame.font.SysFont("Calibri", 20)
        message = font.render(msg, 1, (255, 255, 255)) 
        return message
        
    def focusOnButton(self, window):
        #díky této metodě se tlačítko zvětší pokud na ní bude stát myš
        if self.rect.collidepoint(pygame.mouse.get_pos()):
            window.blit(self.imageLarger, (self.rect[0] - 5, self.rect[1] - 5, self.rect[2], self.rect[3]))
        else:
            window.blit(self.image, self.rect)

    def actionOnPress(self):
        #tato metoda určí co se stane poté, co klikneme na tlačítko
        if self.name == "Randomize":
            self.randomizeShipPositions(pFleet, pGameGrid)
            self.randomizeShipPositions(cFleet, cGameGrid)
        elif self.name == 'Reset':
            self.resetShips(pFleet)
        elif self.name == "Start":
            self.deploymentPhase()
        elif self.name == "New Game":
            self.restartTheGame()
        elif self.name == "Quit":
            pass
        elif self.name == "Help":
            self.printHelpText()
        

    def randomizeShipPositions(self, shiplist, gameGrid): 
        #tato metoda zvolí náhodné pozice lodí na mřízce pro hráče
        if DEPLOYMENT == True:
            randomizeShipPositions(shiplist, gameGrid)

    def resetShips(self, shiplist):
        #vrátí lodě na původní místo, tedy pod mřížku
        if DEPLOYMENT == True:
            for ship in shiplist:
                ship.returnToDefaultPosition()

    def deploymentPhase(self):
        pass

    def restartTheGame(self):
         TOKENS.clear()
         self.resetShips(pFleet)
         self.randomizeShipPositions(cFleet, cGameGrid)
         pGameLogic = createGameLogic(ROWS, COLS)
         cGameLogic = createGameLogic(ROWS, COLS)
    
    def printHelpText(self):
        font1 = pygame.font.SysFont('Calibri',20,True,False)
        font2 = pygame.font.SysFont('Calibri',18,True,False)
        text1 = font1.render('Buttons and their meaning:',True,(255,255,255))
        text2 = font2.render('Quit - exiting the game',True,(255,255,255))
        text3 = font2.render('Randomize - random positions of ships',True,(255,255,255))
        text4 = font2.render('Reset - resets the positions of ships',True,(255,255,255))
        text5 = font2.render('Start - starting a new game',True,(255,255,255))
        GAMESCREEN.blit(text1, (440,340))
        GAMESCREEN.blit(text2, (440,360))
        GAMESCREEN.blit(text3, (440,380))
        GAMESCREEN.blit(text4, (440,400))
        GAMESCREEN.blit(text5, (440,420))
        pygame.display.update()
        pygame.time.wait(5000)
            

    def updateButtons(self, gameStatus):
        #aktualizace tlačítek poté co se na ně klikne
        if self.name == "Start" and gameStatus == False:
            self.name = "New Game"
        elif self.name == "New Game" and gameStatus == True:
            self.name = "Start"
        self.msg = self.addText(self.name)
        self.msgRect = self.msg.get_rect(center=self.rect.center)

    def draw(self, window):
        self.updateButtons(DEPLOYMENT)
        self.focusOnButton(window)
        window.blit(self.msg, self.msgRect)

class Player: 
    def __init__(self): 
        self.turn = True

    def makeAttack(self, grid, logicGrid):
        #když je nařadě hráč, tak musí zaútočit - vybere buňku/políčko druhé mřížky
        posX, posY = pygame.mouse.get_pos()
        if posX >= grid[0][0][0] and posX <= grid[0][-1][0] + 30 and posY >= grid[0][0][1] and posY <= grid[-1][0][1] + 30:
            for i, rowX in enumerate(grid):
                for j, colX in enumerate(rowX):
                    if posX >= colX[0] and posX < colX[0] + 30 and posY >= colX[1] and posY <= colX[1] + 30:
                        if logicGrid[i][j] != ' ':
                            if logicGrid[i][j] == 'O':
                                print("The Player hit Computer's ship")
                                TOKENS.append(Tokens(REDTOKEN, grid[i][j], "The Player hit Computer's ship"))
                                logicGrid[i][j] = 'T'
                                self.turn = False
                        else: 
                            logicGrid[i][j] = "X"
                            print("The Player missed")
                            TOKENS.append(Tokens(GREENTOKEN, grid[i][j], 'The Player missed'))
                            self.turn = False

class Computer: 
    def __init__(self):
        self.turn = False
    
    def makeAttack(self, gamelogic):
        validChoice = False
        while not validChoice:
            rowX = random.randint(0, 9)
            colX = random.randint(0, 9)

            if gamelogic[rowX][colX] == " " or gamelogic[rowX][colX] == "O":
                validChoice = True

        if gamelogic[rowX][colX] == "O":
            print("The Computer hit Player's ship")
            TOKENS.append(Tokens(REDTOKEN, pGameGrid[rowX][colX], "The Computer hit Player's ship"))
            gamelogic[rowX][colX] = "T"
            self.turn = False
            
        else:
            gamelogic[rowX][colX] = "X"
            TOKENS.append(Tokens(GREENTOKEN, pGameGrid[rowX][colX], "The Computer missed"))
            print("The Computer missed")
            self.turn = False
        return self.turn

class Tokens:
    def __init__(self, image, pos, action):
        self.image = image
        self.rect = self.image.get_rect()
        self.pos = pos
        self.rect.topleft = self.pos
        self.action = action
        
    def draw(self, window):
        #tato metoda vykreslí tokens
        window.blit(self.image, self.rect)

#Game utility functions
def createGameGrid(rows, cols, CELLSIZE, pos):
    #vytvoří se souřadnicová mřížka, kde má každá buňka souřadnice
    startX = pos[0]
    startY = pos[1]
    coordGrid = []
    for row in range(rows): # row ma hodnoty od 0 do rows-1
        rowX = []
        for col in range(cols):
            rowX.append((startX, startY))
            startX += CELLSIZE
        coordGrid.append(rowX)
        startX = pos[0]
        startY += CELLSIZE
    return coordGrid 

def createGameLogic(rows, cols):
    #tato funkce aktualizuje stav gameLogic
    gamelogic = []
    for row in range(rows):
        rowX = []
        for col in range(cols):
            rowX.append(" ")
        gamelogic.append(rowX)
    return gamelogic
    
def updateGameLogic(coordGrid, shiplist, gamelogic):
    #tato funkce mění stav hry - gameLogic, prochází pole mřížky - následně jim přídělí T, X  nebo O
    for i, rowX in enumerate(coordGrid):
        for j, colX in enumerate(rowX):
            if gamelogic[i][j] == "T" or gamelogic[i][j] == "X":
                continue
            else:
                gamelogic[i][j] = " "
                for ship in shiplist:
                    if pygame.rect.Rect(colX[0], colX[1], CELLSIZE, CELLSIZE).colliderect(ship.rect):
                        gamelogic[i][j] = "O"


def countRemainingShips(coordGrid, gamelogic):
    #tato funkce spočítá počet částí lodí, které zůstaly v game logic (nejsou potopené)
    remainingShips = 0
    for i, rowX in enumerate(coordGrid):
        for j, colX in enumerate(rowX):
            if gamelogic[i][j] == "O":
                remainingShips += 1
    return remainingShips

def showGridOnScreen(window, CELLSIZE, playerGrid, computerGrid):
#Na obrazovce se objeví obě mřížky
    gamegrids = [playerGrid, computerGrid] 
    for grid in gamegrids: 
        for row in grid: 
            for col in row: 
                pygame.draw.rect(window, (255, 255, 255), (col[0], col[1], CELLSIZE, CELLSIZE), 1)
                                               
def printGameLogic():
#Tato funkce dává oběma mřížkám/tabulkám název při zobrazení v terminálu
    print('Player Grid'.center(50, '#')) #50 v poho
    for _ in pGameLogic:
        print(_)
    print('Computer Grid'.center(50, '#')) #50 v poho
    for _ in cGameLogic:
        print(_)

# def printGrid(coordGrid): #slouží k zobrazneí souřadnic polí mřížek v termínálu
#      for row in coordGrid:
#          for col in row:
#              print(col, end=' ')
#          print()    

def loadImage(path, size, rotate=False):
#Tato funkce importuje obrázek, transformuje ho na určitou velikost, případně ho rotuje o 90 stupňů.  
    img = pygame.image.load(path).convert_alpha()
    img = pygame.transform.scale(img, size)
    if rotate == True:
        img = pygame.transform.rotate(img, -90)
    return img 

def createFleet():
    #vytvoří se flotila lodí
    fleet = []
    for name in FLEET.keys():
        fleet.append(
            Ship(
                 FLEET[name][0], #nazev
                 FLEET[name][1], #souborova cesta
                 FLEET[name][2], #umisteni 
                 FLEET[name][3]) #velikost
        )
    return fleet

def sortFleet(ship, shiplist):
    #tato funkce zajistí, aby se lodě nepřekrývaly
    shiplist.remove(ship)
    shiplist.append(ship)

def randomizeShipPositions(shiplist, gamegrid):
    #tato funkce náhodně vybere úmístění lodí na mřížce
    placedShips = []
    for i, ship in enumerate(shiplist):
        validPosition = False
        while validPosition == False:
            ship.returnToDefaultPosition() #kód níže počítá s tím, že loď je ve vertikální pozici
            rotateShip = random.choice([True, False])
            if rotateShip == True:
                yAxis = random.randint(0, 9)
                xAxis = random.randint(0, 9 - (ship.hImage.get_width()//30))
                ship.rotateShip(True)
                ship.rect.topleft = gamegrid[yAxis][xAxis]
            else:
                yAxis = random.randint(0, 9 - (ship.vImage.get_height()//30))
                xAxis = random.randint(0, 9)
                ship.rect.topleft = gamegrid[yAxis][xAxis]
            if len(placedShips) > 0:
                for item in placedShips:
                    if ship.rect.colliderect(item.rect):
                        validPosition = False
                        break
                    else:
                        validPosition = True
            else:
                validPosition = True
        placedShips.append(ship)

def deploymentPhase(deployment):
    if deployment == True:
        return False
    else:
        return True
    
def takeTurns(p1, p2):
    if p1.turn == True:
        p2.turn = False
    else:
        p2.turn = True
        if not p2.makeAttack(pGameLogic):
            p1.turn = True
        
def updateGameScreen(window):
#Tato funkce aktualizuje obrazovku
    window.fill((0, 0, 0))

    window.blit(PGAMEGRIDIMG, (0, 0)) #pozadí mřížky hráče
    window.blit(CGAMEGRIDIMG, (cGameGrid[0][0][0] - 30, cGameGrid[0][0][1] - 30)) #pozadí mřížky počítače

    #Zobrazení obou mřížek/tabulek - playerGrid a computerGrid
    showGridOnScreen(window, CELLSIZE, pGameGrid, cGameGrid)

    #Vykreslení (seznam) lodí pro hráče (pod pGameGrid) na obrazovku
    for ship in pFleet:
        ship.draw(window) #kdyby tento řádek byl jako poslední, loď by se hned zarovnávala do mřížek
        ship.snapToGridEdge(pGameGrid)
        ship.snapToGrid(pGameGrid)

    for ship in cFleet: 
        #ship.draw(window) #schovám lodě počítače - nebudou se vykreslovat
        ship.snapToGridEdge(cGameGrid)
        ship.snapToGrid(cGameGrid)

    for button in BUTTONS:
        button.draw(window)

    for token in TOKENS:
        token.draw(window)
    
    updateGameLogic(pGameGrid, pFleet, pGameLogic)
    updateGameLogic(cGameGrid, cFleet, cGameLogic)

    pygame.display.update()

#Game settings and varriables
SCREENWIDTH = 756
SCREENHEIGHT = 600
ROWS = 10
COLS = 10
CELLSIZE = 30
DEPLOYMENT = True

#Pygame display initilazation #Vytvoření GAMESCREEN, názvu a ikony aplikace 
GAMESCREEN = pygame.display.set_mode((SCREENWIDTH, SCREENHEIGHT))
pygame.display.set_caption('Lodě (Battleship)')
icon = pygame.image.load("assets/icon/icon_transparentn.png")
pygame.display.set_icon(icon)

#Game lists
FLEET = {"battleship": ["battleship", "assets/images/ships/battleship/battleship.png",
                        (95, 380), (24, 117)],
        "cruiser": ["cruiser", "assets/images/ships/cruiser/cruiser.png",
                    (140, 380), (24, 117)],
        "destroyer": ["destroyer", "assets/images/ships/destroyer/destroyer.png",    
                      (185, 380), (18, 87)],
        "patrol boat": ["patrol boat", "assets/images/ships/patrol boat/patrol boat.png",
                        (275, 380), (12, 57)],
        "submarine": ["submarine", "assets/images/ships/submarine/submarine.png",
                      (230, 380), (18, 87)],
        "carrier": ["carrier", "assets/images/ships/carrier/carrier.png",
                    (50, 380), (27, 147)],
        "rescue ship": ["rescue ship", "assets/images/ships/rescue ship/rescue ship.png", 
                        (320, 380), (12, 57)]
        }

#Loading game variables
pGameGrid = createGameGrid(ROWS, COLS, CELLSIZE, (30, 30))
pGameLogic = createGameLogic(ROWS, COLS)
pFleet = createFleet()

cGameGrid = createGameGrid(ROWS, COLS, CELLSIZE, (SCREENWIDTH - (ROWS * CELLSIZE), 30))
cGameLogic =createGameLogic(ROWS, COLS) 
cFleet = createFleet()
randomizeShipPositions(cFleet, cGameGrid)

printGameLogic() #printGrid(pGameGrid) #slouží k zobrazení souřadnic polí mřížek v termínálu

#Loading images
PGAMEGRIDIMG = loadImage("assets/images/grids/player_grid.png", ((ROWS + 1) * CELLSIZE, (COLS + 1) * CELLSIZE)) #pozadí mřížky hráče
CGAMEGRIDIMG = loadImage("assets/images/grids/comp_grid.png", ((ROWS + 1) * CELLSIZE, (COLS + 1) * CELLSIZE)) #pozadí mřížky počítače

BUTTONIMAGE = loadImage("assets/images/buttons/button.png", (120, 40))
BUTTONS = [
    Button(BUTTONIMAGE, (120, 40), (25, 545), "Quit"),
    Button(BUTTONIMAGE, (120, 40), (170, 545), "Randomize"),
    Button(BUTTONIMAGE, (120, 40), (317.5, 545), "Reset"),
    Button(BUTTONIMAGE, (120, 40), (465, 545), "Help"), 
    Button(BUTTONIMAGE, (120, 40), (610, 545), "Start"),
    
]


REDTOKEN = loadImage("assets/images/tokens/redtoken.png", (CELLSIZE, CELLSIZE))
GREENTOKEN = loadImage("assets/images/tokens/greentoken.png", (CELLSIZE, CELLSIZE))

TOKENS = []

#Initialise players
player1 = Player()
computer = Computer() 

#End Game Results
def printResults():
    font = pygame.font.SysFont('Calibri',25,True,False)
    text = font.render(winner+' Won!',True,(255,255,255))
    GAMESCREEN.blit(text, (310,380))
    pygame.display.update()

#Main game loop
RUNGAME = True
GAMEOVER = False
while RUNGAME:

    if GAMEOVER == True:
        printResults()

        while GAMEOVER == True:
            for event in pygame.event.get():   
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    for button in BUTTONS:
                        if button.rect.collidepoint(pygame.mouse.get_pos()):
                                
                            if button.name == 'Reset':
                                DEPLOYMENT = True
                                button.restartTheGame()
                                pGameLogic = createGameLogic(ROWS, COLS)
                                cGameLogic = createGameLogic(ROWS, COLS)
                                updateGameScreen(GAMESCREEN)
                                #GAMESCREEN.fill((0, 0, 0))
                                GAMEOVER = False
                                print(GAMEOVER)
                            #button.actionOnPress()

                            elif button.name == 'New Game':
                                DEPLOYMENT = True
                                button.restartTheGame()
                                pGameLogic = createGameLogic(ROWS, COLS)
                                cGameLogic = createGameLogic(ROWS, COLS)
                                updateGameScreen(GAMESCREEN)
                                #GAMESCREEN.fill((0, 0, 0))
                                GAMEOVER = False
                                print("new game")
                                print(GAMEOVER)

                            elif button.name == "Quit":
                                pygame.quit()
                                

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            RUNGAME = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                if DEPLOYMENT == True:
                    for ship in pFleet:
                        if ship.rect.collidepoint(pygame.mouse.get_pos()):
                            ship.active = True
                            sortFleet(ship, pFleet)
                            ship.selectShipAndMove()
    
                else:
                    if player1.turn == True:
                        player1.makeAttack(cGameGrid, cGameLogic)
                        if countRemainingShips(cGameGrid,cGameLogic) == 0:
                            print ("Player wins")
                            break

                for button in BUTTONS:
                    if button.rect.collidepoint(pygame.mouse.get_pos()):
                        
                        if button.name == 'Start':
                            if countRemainingShips(pGameGrid,pGameLogic) == 23:
                                status = deploymentPhase(DEPLOYMENT)
                                DEPLOYMENT = status

                        elif button.name == "New Game":
                            DEPLOYMENT = True
                            button.restartTheGame()
                            pGameLogic = createGameLogic(ROWS, COLS)
                            cGameLogic = createGameLogic(ROWS, COLS)
                            updateGameScreen(GAMESCREEN)
                            #GAMESCREEN.fill((0, 0, 0))
                            GAMEOVER = False
                            print("new game")
                            print(GAMEOVER)
                            
                        elif button.name == "Quit":
                            if DEPLOYMENT == True:
                                    RUNGAME = False
                            elif DEPLOYMENT == False:
                                    RUNGAME = False
                            elif GAMEOVER == True:
                                    RUNGAME = False
                            elif GAMEOVER == False:
                                    RUNGAME = False
                            
                        elif button.name == "Help":
                            print()
                            
                        button.actionOnPress()  

            elif event.button == 2:
                printGameLogic()

            elif event.button == 3:
                if DEPLOYMENT == True:
                    for ship in pFleet:
                        if ship.rect.collidepoint(pygame.mouse.get_pos()) and not ship.checkForRotateCollisions(pFleet):
                            ship.rotateShip(True)

    updateGameScreen(GAMESCREEN)

    takeTurns(player1, computer)
    if DEPLOYMENT == False:
        if countRemainingShips(pGameGrid,pGameLogic) == 0:
                winner = "Computer"
                GAMEOVER = True
                print("Computer wins")
                print(pGameLogic)
                print(cGameLogic)
                updateGameScreen(GAMESCREEN)
        if countRemainingShips(cGameGrid,cGameLogic) == 0:
                winner = "Player"
                GAMEOVER = True
                print("Player wins")
                print(cGameLogic)
                print(pGameLogic)

pygame.quit()

